(cons* ;; (channel
       ;;  (name 'backtick)
       ;;  (url "https://gitlab.com/backtick/guixies.git")
       ;;  (branch "slave"))
       (channel
        (name 'guixies)
        (url "file:///home/camel/git/guixies")
        (branch "slave"))
       (channel
        (name 'nonguix)
        (url "https://gitlab.com/nonguix/nonguix")
        ;; Enable signature verification:
        (introduction
         (make-channel-introduction
          "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
          (openpgp-fingerprint
           "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
       (channel
        (name 'rust-team)
        (url "https://git.savannah.gnu.org/git/guix.git")
        ;; Enable signature verification:
        (introduction
         (make-channel-introduction
          "9edb3f66fd807b096b48283debdcddccfea34bad"
          (openpgp-fingerprint
           "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA"))))
       (channel
        (name 'akagi)
        (url "https://git.sr.ht/~akagi/rrr")
        (introduction
         (make-channel-introduction
          "794d6e5eb362bfcf81ada12b6a49a0cd55c8e031"
          (openpgp-fingerprint
           "FF72 877C 4F21 FC4D 467D  20C4 DCCB 5255 2098 B6C1"))))
       %default-channels)
