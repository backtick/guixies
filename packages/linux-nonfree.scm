;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2012, 2013, 2014, 2015 Ludovic CourtÃ¨s <ludo@gnu.org>
;;; Copyright © 2013, 2014 Andreas Enge <andreas@enge.fr>
;;; Copyright © 2012 Nikita Karetnikov <nikita@karetnikov.org>
;;; Copyright © 2014, 2015 Mark H Weaver <mhw@netris.org>
;;; Copyright © 2015 Federico Beffa <beffa@fbengineering.ch>
;;; Copyright © 2015 Taylan Ulrich BayÄ±rlÄ±/Kammer <taylanbayirli@gmail.com>
;;; Copyright © 2015 Andy Wingo <wingo@igalia.com>
;;; Copyright © 2015 Eric Dvorsak <eric@dvorsak.fr>
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (linux-nonfree)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages compression)
  #:use-module (guix build-system trivial)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match))

;;; Forgive me mr. Stallman for I have sinned.

(define-public firmware-nonfree
  (package
    (name "firmware-nonfree")
    (version "1ba3519eab0fbc3cedf6b423ea0470461b902c1b")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url
"https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git")
                    (commit version)))
              (sha256
               (base32
                "01hpam2xmhd5rhijqg7h6qn0bnn3zvizb10zc5c2s8pm9b6ccfm7"))))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
                 #:builder (begin
                             (use-modules (guix build utils))
                             (let ((source (assoc-ref %build-inputs "source"))
                                   (fw-dir (string-append %output "/lib/firmware/")))
                               (mkdir-p fw-dir)
                               (copy-recursively source fw-dir)
                               #t))))
    (home-page "https://kernel.org/")
    (synopsis "Non-free firmware")
    (description "Non-free firmware")
    ;; FIXME: What license?
    (license license:lgpl2.1+)))

(define (linux5-nonfree-urls version)
  "Return a list of URLs for Linux-Nonfree VERSION."
  (list (string-append
         "https://cdn.kernel.org/pub/linux/kernel/v5.x/"
         "linux-" version ".tar.xz")))

(define (linux-nonfree-urls version)
  "Return a list of URLs for Linux-Nonfree VERSION."
  (list (string-append
         "https://cdn.kernel.org/pub/linux/kernel/v6.x/"
         "linux-" version ".tar.xz")))

(define-public linux-nonfree
  (let* ((version "5.16.20"))
    (package
      (inherit linux-libre)
      (name "linux-nonfree")
      (version version)
      (source (origin
                (method url-fetch)
                (uri (linux5-nonfree-urls version))
                (sha256
                 (base32
                  "09dz8zp8cxvsc5amrswqqrkxd3i92ay2samlcspalaw6iz40s1nq"))))
      (synopsis "Mainline Linux kernel, nonfree binary blobs included.")
      (description "Linux is a kernel.")
      (license license:gpl2)
      (home-page "http://kernel.org/"))))

(define-public perf-nonfree
  (package
    (inherit perf)
    (name "perf-nonfree")
    (version (package-version linux-nonfree))
    (source (package-source linux-nonfree))
    (license (package-license linux-nonfree))))

(define %reiser4-linux-options
  `(("CONFIG_REISER4_FS" . m)))

(define-public linux-reiser4
  (package
    (inherit
     (customize-linux #:name "linux-reiser4"
                      #:configs (string-join (map (match-lambda
                                                   ((option . 'm)
                                                    (string-append option "=m"))
                                                   ((option . #t)
                                                    (string-append option "=y"))
                                                   ((option . #f)
                                                    (string-append option "=n")))
                                                   %reiser4-linux-options) "\n")
                      #:source (origin
                                 (method url-fetch)
                                 (uri (linux-nonfree-urls version))
                                 (sha256
                                  (base32
                                   "09dz8zp8cxvsc5amrswqqrkxd3i92ay2samlcspalaw6iz40s1nq"))
                                 (patches (list (origin
                                                  (method url-fetch)
                                                  (uri "https://gitlab.com/backtick/guixies/-/raw/slave/packages/patches/reiser4-for-5.16.patch")
                                                  (sha256
                                                   (base32
                                                    "1fdhrndlq2p6c6awjslk54zqq24wy7kxjwdq5jl3qgz8ls1zx2k5"))))))))
    (version "5.16.20")
    (synopsis "Linux with Reiser4 patch.")
    (description "Linux-Reiser4 is a kernel that supports Reiser4 filesystem.")
    (license license:gpl2)
    (home-page "https://reiser4.wiki.kernel.org/index.php/Main_Page")))

(define %reiser5-linux-options
  `(("CONFIG_REISER4_FS" . m)))

(define-public linux-reiser5
  (package
    (inherit
     (customize-linux #:name "linux-reiser5"
                      #:configs (string-join (map (match-lambda
                                                   ((option . 'm)
                                                    (string-append option "=m"))
                                                   ((option . #t)
                                                    (string-append option "=y"))
                                                   ((option . #f)
                                                    (string-append option "=n")))
                                                   %reiser5-linux-options) "\n")
                      #:source (origin
                                 (method url-fetch)
                                 (uri (linux-nonfree-urls version))
                                 (sha256
                                  (base32
                                   "09dz8zp8cxvsc5amrswqqrkxd3i92ay2samlcspalaw6iz40s1nq"))
                                 (patches (list (origin
                                                  (method url-fetch)
                                                  (uri "https://gitlab.com/backtick/guixies/-/raw/slave/packages/patches/reiser5-for-5.16.patch")
                                                  (sha256
                                                   (base32
                                                    "1y7y1d1jk3giigg7bl7a9sriz62xw93mj5vimdqj8z4vbdcspgpl"))))))))
    (version "5.16.20")
    (synopsis "Linux with Reiser5 patch.")
    (description "Linux-Reiser5 is a kernel that supports Reiser5 filesystem.")
    (license license:gpl2)
    (home-page "https://reiser4.wiki.kernel.org/index.php/Main_Page")))

(define %reiser4-rt-linux-options
  `(;("CONFIG_PREEMPT_LAZY" . #t)
    ("CONFIG_PREEMPT_VOLUNTARY" . #f)
    ;("CONFIG_PREEMPT_RT" . #t)
    ("CONFIG_REISER4_FS" . m)))

(define-public linux-reiser4-rt
  (package
    (inherit
     (customize-linux #:name "linux-reiser4-rt"
                      ;; #:configs (string-join (map (match-lambda
                      ;;                              ((option . 'm)
                      ;;                               (string-append option "=m"))
                      ;;                              ((option . #t)
                      ;;                               (string-append option "=y"))
                      ;;                              ((option . #f)
                      ;;                               (string-append option "=n")))
                      ;;                             %reiser4-rt-linux-options) "\n")
                      #:configs ('(\"CONFIG_PREEMPT_LAZY=y\"
                                ;   \"# CONFIG_PREEMPT_VOLUNTARY is not set\"
                                   \"CONFIG_PREEMPT_RT=y\"
                                   \"CONFIG_REISER4_FS=m\"))
                      #:source (origin
                                 (method url-fetch)
                                 (uri (linux-nonfree-urls version))
                                 (sha256
                                  (base32
                                   "09dz8zp8cxvsc5amrswqqrkxd3i92ay2samlcspalaw6iz40s1nq"))
                                 (patches (list (origin
                                                  (method url-fetch)
                                                  (uri "https://gitlab.com/backtick/guixies/-/raw/slave/packages/patches/linux-rt-5.16.20.patch")
                                                  (sha256
                                                   (base32
                                                    "1gwamjrli284f55c6jg2qsp3m64n3lz1arwsv0gcwpcb81a09i5y")))
                                                (origin
                                                  (method url-fetch)
                                                  (uri "https://gitlab.com/backtick/guixies/-/raw/slave/packages/patches/reiser4-for-5.16.patch")
                                                  (sha256
                                                   (base32
                                                    "1fdhrndlq2p6c6awjslk54zqq24wy7kxjwdq5jl3qgz8ls1zx2k5"))))))))
    (version "5.16.20")
    (synopsis "Linux with Reiser4 and RTPreempt patch.")
    (description "Linux-Reiser4 is a real-time kernel that supports Reiser4 filesystem.")
    (license license:gpl2)
    (home-page "https://reiser4.wiki.kernel.org/index.php/Main_Page")))

(define %reiser5-rt-linux-options
  `(("CONFIG_PREEMPT_RT" . #t)
    ("CONFIG_REISER4_FS" . m)))

(define-public linux-reiser5-rt
  (package
    (inherit
     (customize-linux #:name "linux-reiser5-rt"
                      #:configs (string-join (map (match-lambda
                                                   ((option . 'm)
                                                    (string-append option "=m"))
                                                   ((option . #t)
                                                    (string-append option "=y"))
                                                   ((option . #f)
                                                    (string-append option "=n")))
                                                   %reiser5-rt-linux-options) "\n")
                      #:source (origin
                                 (method url-fetch)
                                 (uri (linux-nonfree-urls version))
                                 (sha256
                                  (base32
                                   "09dz8zp8cxvsc5amrswqqrkxd3i92ay2samlcspalaw6iz40s1nq"))
                                 (patches (list (origin
                                                  (method url-fetch)
                                                  (uri "https://gitlab.com/backtick/guixies/-/raw/slave/packages/patches/linux-rt-5.16.20.patch")
                                                  (sha256
                                                   (base32
                                                    "1gwamjrli284f55c6jg2qsp3m64n3lz1arwsv0gcwpcb81a09i5y")))
                                                (origin
                                                  (method url-fetch)
                                                  (uri "https://gitlab.com/backtick/guixies/-/raw/slave/packages/patches/reiser5-for-5.16.patch")
                                                  (sha256
                                                   (base32
                                                    "1y7y1d1jk3giigg7bl7a9sriz62xw93mj5vimdqj8z4vbdcspgpl"))))))))
    (version "5.16.20")
    (synopsis "Linux with Reiser5 and RTPreempt patch.")
    (description "Linux-Reiser5 is a real-time kernel that supports Reiser5 filesystem.")
    (license license:gpl2)
    (home-page "https://reiser4.wiki.kernel.org/index.php/Main_Page")))
