;; A package variant that may be out of date and vulnerable. Only for use in
;; test suites and should never be referred to by a built package.
(define-module (httpd)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (gnu packages web))

(define-public httpd/pinned
  (hidden-package
   (package
     (inherit httpd)
     (version "2.4.52")
     (source (origin
               (method url-fetch)
               (uri (string-append "https://archive.apache.org/dist/httpd/httpd-"
                                   version ".tar.bz2"))
               (sha256
                (base32
                 "1jgmfbazc2n9dnl7axhahwppyq25bvbvwx0lqplq76by97fgf9q1")))))))
